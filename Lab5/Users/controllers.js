const express = require('express');

const UsersService = require('./services.js');
const {
    validateFields
} = require('../utils');

const router = express.Router();

router.post('/roles', async (req, res, next) => {
    const {
        value
    } = req.body;

    try {
	const fieldsToBeValidated = {
            value: {
                value: req.query.value,
                type: 'alpha'
            }
        };
        validateFields(fieldsToBeValidated);
	console.log("POST ROLES");
	console.log(req.query.value);
        await UsersService.addRole(req.query.value);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
    
});

//ruta pt verificarea datelor
router.get('/roles', async (req, res, next) => {
    res.json(await UsersService.getRoles());
});

router.post('/register', async (req, res, next) => {
    const {
        username,
        password,
        role_id
    } = req.body;

    //validare de campuri
    try {
        const fieldsToBeValidated = {
            username: {
                value: req.query.username,
                type: 'alpha'
            },
            password: {
                value: req.query.password,
                type: 'ascii'
            },
            role_id: {
                value: parseInt(req.query.role_id),
                type:'int'
            }
        };
	console.log(parseInt(req.query.role_id));
        validateFields(fieldsToBeValidated);

        await UsersService.add(req.query.username, req.query.password, parseInt(req.query.role_id));

        res.status(201).end();
    } catch (err) {

        next(err);
    }
});

//ruta pt verificarea datelor
router.get('/', async (req, res, next) => {
    res.json(await UsersService.getUsers());
});

router.post('/login', async (req, res, next) => {
  const {
      username,
      password
  } = req.body;

  try {
    const fieldsToBeValidated = {
        username: {
            value: req.query.username,
            type: 'alpha'
        },
        password: {
            value: req.query.password,
            type: 'ascii'
        }
    };

    validateFields(fieldsToBeValidated);

    const token = await UsersService.authenticate(req.query.username, req.query.password);

    res.status(200).json(token);
} catch (err) {
    next(err);
}

})
module.exports = router;


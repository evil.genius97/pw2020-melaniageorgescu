const {query} = require('../data');

const add = async (name, author_id) => {
    await query('INSERT INTO books (name, author_id) VALUES ($1, $2)', [name, author_id]);
};

const getAll = async () => {
    return await query('SELECT a.id, a.name, b.first_name, b.last_name FROM books a, authors b WHERE a.author_id = b.id');
};


const getById = async (id) => {
    return await query('SELECT a.id, a.name, b.first_name, b.last_name FROM books a, authors b WHERE a.id = $1 AND a.author_id=b.id', [id]);
};

const updateById = async (id, name, author_id) => {
    await query('UPDATE books SET name = $1, author_id = $2 WHERE id = $3', [name, author_id, id]);
};

const deleteById = async (id) => {
    await query('DELETE FROM books WHERE id = $1', [id]);
};

const addPublisher = async price => {
    await query('INSERT INTO publishers_books (price) VALUES ($1)', [price]);
  };
  
  const getAllPublishers = async () => {
    return await query('SELECT * FROM publishers_books');
  };

module.exports = {
    add,
    getAll,
    getById,
    updateById,
    deleteById,
    addPublisher,
    getAllPublishers
}

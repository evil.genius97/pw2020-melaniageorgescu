const express = require('express');
const myModule = require('./database.js');
const router = require('./ex.js');

const bodyParser     =        require("body-parser"); 
const app = express();
 

app.use('/hello', router);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(3000);

const express = require('express');
const myModule = require('./database.js');

var bodyParser     =        require("body-parser"); 
var app = express();
 
const router = express.Router();
app.use('/hello', router);
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


router.post('/insert', (req, res) => {
	const body = JSON.stringify(req.body);
	const book = {
	title : String,
	author : String  
	}
	book.title = req.query.title;
	book.author = req.query.author;
	myModule.insertIntoDb(book);
	res.json(book);
	console.log("insert");
});


router.get('/getId/:id', (req, res) => { 
   const paramId = req.params.id;
   res.send(myModule.getFromDbById(paramId));
});

router.get('/getAuthor', (req, res) => { 
	const author = req.query.author;
	console.log(req.query.author);
	res.send(myModule.getFromDbByAuthor(author)); 
});


router.get('/allBooks', (req, res) => { 
  res.send(myModule.getAllFromDb());
});

router.delete('/delbyId', (req, res) => {
	const author = req.query.author;
	db.removeFromDbByAuthor(author);
	res.send('Id Deleted');
       

});

router.delete('/delAuthor', (req, res) => {
	const author = req.query.author;
	myModule.removeFromDbByAuthor(author);
	res.send('Deleted');
});

router.delete('/delAll', (req, res) => {
	const author = req.query.author;
	myModule.purgeDb();
	res.send('Deleted All');
});



module.exports = router;
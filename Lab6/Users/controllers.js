const express = require('express');

const UsersService = require('./services.js');
const { validateFields } = require('../utils');

const router = express.Router();

router.post('/register', async (req, res, next) => {
    const {
        username,
        password
    } = req.body;

    //validare de campuri
    try {
        const fieldsToBeValidated = {
            username: {
                value: username,
                type: 'alpha'
            },
            password: {
                value: password,
                type: 'ascii'
            }
        };
        validateFields(fieldsToBeValidated);
        await UsersService.add(username, password);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
  const {
      username,
      password
  } = req.body;

  try {
    const fieldsToBeValidated = {
        username: {
            value: username,
            type: 'alpha'
        },
        password: {
            value: password,
            type: 'ascii'
        }
    };

    validateFields(fieldsToBeValidated);

    const token = await UsersService.authenticate(username, password);

    res.status(200).json(token);
} catch (err) {
    next(err);
}

})
module.exports = router;
import React from 'react';
import logo from './logo.svg';
import './App.css';
import Counter from './Counter.js';
import Layout from './Layout.js';

function App() {
  return (
    <div> 
      <Layout > </Layout>       
    </div>
  );
}


export default App;

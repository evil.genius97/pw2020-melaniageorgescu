import React from 'react';

function Nav() {
    return (
      <div className="App-nav">
        <ul id="menu">
          <img src="https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/Chrome__logo.max_2800x2800.png"></img>
          <li><a href="#">Home</a></li>
          <li><a href="#">Books</a></li>
          <li><a href="#">Authors</a></li>
          <div class="dropdown">
          <button class="dropbtn">Dropdown</button>
          <div class="dropdown-content">
            <button href="#">Logout</button>
          </div>
        </div>
        </ul>
        
      </div>
    );
  }


export default Nav;
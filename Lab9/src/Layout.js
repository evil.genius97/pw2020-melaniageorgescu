import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Nav from './Nav';
import Counter from './Counter';

class Layout extends React.Component {
    render() {
        return (
           
           <div>
                <Header>
                    <h1 >This is the header</h1>
                </Header>
                <Nav title='This is the nav section' bodyText='Another day in isolation. Everything is terrible and I am very sad.'/>
                <Counter></Counter> 
                <Footer>
                    <h1 >This is the footer. Byeee!</h1>
                </Footer> 
                </div>   
           
        )
    }
}

export default Layout;

const express = require('express');

const BooksService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const authors = await BooksService.getAll();
        res.json(authors);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    const { id } = req.params;
  
    try {
      validateFields({
        id: {
          value: id,
          type: 'int'
        }
      });
  
      const book = await BooksService.getById(parseInt(id));
      res.json(book);
    } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
    }
  });

router.post('/', async (req, res, next) => {
    const {
        name,
        author_id
    } = req.body;

    try {

        const fieldsToBeValidated = {
            name: {
                value: name,
                type: 'alpha'
            },
            author_id: {
                value: parseInt(req.query.author_id),
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);
        await BooksService.add(req.query.name, parseInt(req.query.author_id));
        res.status(201).send('Insert ok!' );
    } catch (err) {
        next(err);
    }
});

router.put('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        name,
        author_id
    } = req.body;
    try {

        const fieldsToBeValidated = {
            id: {
                value: parseInt(id),
                type: 'int'
            },
            name: {
                value: req.query.name,
                type: 'ascii'
            },
            author_id: {
                value: parseInt(req.query.author_id),
                type: 'int'
            }
        };
        validateFields(fieldsToBeValidated);
        await BooksService.updateById(parseInt(id), req.query.name, parseInt(req.query.author_id));
        res.status(201).send('Update ok!' );
    } catch (err) { 
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        await BooksService.deleteById(parseInt(id));
        res.status(204).send('Delete ok!');
    } catch (err) {
        next(err);
    }
});

module.exports = router;
